import xml.etree.ElementTree as ET
import random

# Creating the root of the structure (Element class)
# <class class_id='SKJ-2021S'>...</class>
root = ET.Element('class', class_id='SKJ-2021S')

student_names = [
    'ABC0123', 'DEF4567', 'GHI8901', 'JKL2345', 'MNO6789', 'PQR0123']
for student_name in student_names:
    # Adding each student to the root of the structure
    # <student student_id="ABC0123">...</student>
    student = ET.SubElement(root, 'student', student_id=student_name)
    for task_id in range(8):
        # Creating tasks for each student with evaluation
        # There are 8 tasks in the subject
        # <task task_id="1">...</task>
        task = ET.SubElement(student, 'task', task_id=str(task_id + 1))

        # Setting the value / text of the tag to a random value between 0 and 5
        # <task task_id="8">3</task>
        task.text = str(random.randint(0, 5))

# Wrapping the Element class into the ElementTree class,
# which supports serialization to a file
tree = ET.ElementTree(root)
tree.write('skj_class.xml')
