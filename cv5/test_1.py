import xml.etree.ElementTree as ET
import pytest

from tasks import (
    create_student, remove_student, set_task_points, create_task, remove_task
)

INPUT_XML = (
    '<class class_id="SKJ-2021S">'
    '<student student_id="ABC0123"><task task_id="1">4</task>'
    '<task task_id="2">4</task><task task_id="3">4</task>'
    '<task task_id="4">0</task><task task_id="5">4</task>'
    '<task task_id="6">5</task><task task_id="7">5</task>'
    '<task task_id="8">0</task></student>'
    '<student student_id="DEF4567"><task task_id="1">4</task>'
    '<task task_id="2">4</task><task task_id="3">4</task>'
    '<task task_id="4">4</task><task task_id="5">3</task>'
    '<task task_id="6">2</task><task task_id="7">1</task>'
    '<task task_id="8">1</task></student>'
    '<student student_id="GHI8901"><task task_id="1">1</task>'
    '<task task_id="2">2</task><task task_id="3">3</task>'
    '<task task_id="4">2</task><task task_id="5">3</task>'
    '<task task_id="6">4</task><task task_id="7">4</task>'
    '<task task_id="8">4</task></student>'
    '<student student_id="JKL2345"><task task_id="1">4</task>'
    '<task task_id="2">1</task><task task_id="3">2</task>'
    '<task task_id="4">5</task><task task_id="5">0</task>'
    '<task task_id="6">5</task><task task_id="7">5</task>'
    '<task task_id="8">4</task></student>'
    '<student student_id="MNO6789"><task task_id="1">2</task>'
    '<task task_id="2">1</task><task task_id="3">4</task>'
    '<task task_id="4">0</task><task task_id="5">2</task>'
    '<task task_id="6">5</task><task task_id="7">2</task>'
    '<task task_id="8">2</task></student>'
    '<student student_id="PQR0123"><task task_id="1">0</task>'
    '<task task_id="2">2</task><task task_id="3">2</task>'
    '<task task_id="4">3</task><task task_id="5">1</task>'
    '<task task_id="6">4</task><task task_id="7">1</task>'
    '<task task_id="8">5</task></student>'
    '</class>'
)


def test_create_student():
    root = ET.fromstring(INPUT_XML)
    original_length = len([s.attrib['student_id'] for s in root])

    create_student(root, "TST0000")
    student_ids = [s.attrib['student_id'] for s in root]
    assert len(student_ids) == original_length + 1
    assert "TST0000" in student_ids

    with pytest.raises(Exception, match="^student already exists$"):
        create_student(root, "ABC0123")


def test_remove_student():
    root = ET.fromstring(INPUT_XML)
    original_length = len([s.attrib['student_id'] for s in root])

    remove_student(root, "ABC0123")
    student_ids = [s.attrib['student_id'] for s in root]
    assert len(student_ids) == original_length - 1
    assert "ABC0123" not in student_ids


def test_set_task_points():
    root = ET.fromstring(INPUT_XML)
    set_task_points(root, 'ABC0123', '1', '5')
    student = root.find(".//student[@student_id='ABC0123']")
    task_points = int(student.find(".//task[@task_id='1']").text)
    assert task_points == 5


def test_create_task():
    root = ET.fromstring(INPUT_XML)
    create_task(root, 'PQR0123', '9', '5')
    student = root.find(".//student[@student_id='PQR0123']")
    assert len(list(student)) == 9
    task = student.find(".//task[@task_id='9']")
    assert task.text == '5'


def test_remove_task():
    root = ET.fromstring(INPUT_XML)
    remove_task(root, '3')
    task = root.find(".//task[@task_id='3']")
    assert task is None
