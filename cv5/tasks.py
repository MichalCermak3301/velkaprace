import xml.etree.ElementTree as ET


def create_student(xml_root, student_id):
    if xml_root.find("student[@student_id='{}']".format(student_id)):
        raise Exception('student already exists')
    se = ET.SubElement(xml_root, "student", attrib={"student_id": student_id})
    return se


def remove_student(xml_root, student_id):
    se = xml_root.find("student[@student_id='{}']".format(student_id))
    if se is None:
        return
    xml_root.remove(se)
    pass


def set_task_points(xml_root, student_id, task_id, points):
    se = xml_root.find("student[@student_id='{}']".format(student_id))
    if se is None:
        raise Exception('Student does not exit')
    te = se.find("task[@task_id='{}']".format(task_id))
    if te is None:
        raise Exception("Task does not exist")
    te.text = str(points)
    pass


def create_task(xml_root, student_id, task_id, points):
    se = xml_root.find("student[@student_id='{}']".format(student_id))
    if se is None:
        raise Exception('student does not exist')
    if se.find("task[@task_id='{}']".format(task_id)) is not None:
        raise Exception('task already exists')
    te = ET.SubElement(se, "task", attrib={"task_id": task_id})
    te.text = str(points)


def remove_task(xml_root, task_id):
    for se in xml_root.findall("student"):
        te = se.find("task[@task_id='{}']".format(task_id))
        if te is not None:
            se.remove(te)
    pass
