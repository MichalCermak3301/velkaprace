
# Závěrečná práce

Autoři: KLE0197, CER0549, MAT0513

Požadavky na kvalitu práce (hodnotící kritéria)

* [x] GIT repozitář na Gitlabu.
  * [x] Repozitář má README.md
* [x] Použití vlastního projektu.
* [x] Vytvořená CI v repozitáři.
* [x] CI má minimálně tři úlohy
  * [x] Test kvality Markdown stránek.
  * [x] Kontrola syntaxe každého jazyka.
  * [x] Použití "lint" nástroje pro každý jazyk.
* [ ] (Volitelné/doporučené)
Nástroj na kontrolu dostupnosti aktualizací/security updates pro moduly jazyka.
* [x] (Volitelné/doporučené) Nástroj na kontrolu testů/code coverage.
* [x] (Volitelné/doporučené) Pokud některé nástroje umí generovat HTML reporty.
* [ ] (Volitelné/doporučené) Repozitář obsahuje šablonu ticketu.
